# Install Party

## Resum Install Party

La **Install Party** és un esdeveniment organitzat per l'Associació d'estudiants LinuxUPC. Consisteix en
la reunió dels estudiants nouvinguts al món universitari amb els membres de l'associació, amb la finalitat
introduir als nous estudiants al món del programari lliure. Més concretament els membres de l'associació
ajudarem als estudiants a instal·lar-se alguna distribució de GNU/Linux al seu dispositiu.

La resta del document està estructurat de la següent forma: primer a la secció [Pre Install Party](#pre-install-party) tractarem
les coses importants que s'han de fer i que s'han de tenir en compte abans de l'esdeveniment; després a la secció
d'[Instal·lació](#installació) parlarem de com es du a terme la instal·lació en concret, tenint en compte les necessitats de cada
estudiant; finalment a les [Recomanacions](#recomanacions) parlarem de quins són els passos recomanats després de la instal·lació
perquè es pugui gaudir de la distribució acabada d'instal·lar al 100%. A més a més al final de tot podràs trobar una secció de [resolució de problemes](#resolució-de-problemes).

## Pre Install Party

<!--Que fer abans de venir a la Install Party-->

### Recomanacions de Distros

<!--Decidir 4 distros per recomanar i les seves característiques-->

| **Distribució** | Nivell      | Estavilitat             | [Bloated](https://en.wikipedia.org/wiki/Software_bloat)    |
|-----------------|-------------|-------------------------|---------|
| **Ubuntu**      | Principiant | Alta                    | Big     |
| **Manjaro**     | Baix        | Alta                    | Big     |
| **Debian**      | Mitja       | Molt Alta               | Medium  |
| **Archlinux**   | Alt         | Rolling release         | Minimal |

#### Ubuntu

#### Manjaro

#### Debian

#### Archlinux


## Instal·lació
### Distribució d'imatges
### Crear espai per Linux
### Boot des de USB

<!--Buscar tecla per diferents fabricants-->

### Connexió a Internet

The best thing is to have a cable connection (with USB network sharing alsoworks).

### Instal·lació guiada

First check if its uefi or bios.

### Ubuntu

1. Once you boot select the lenguage of the installation process, and then select "Probar Ubuntu".
2. Chose a network.
3. Start the installation.
4. Select layout/language.
5. Install third party software. And continue.
6. Install next to Windows 10. (If you want to keep Windows).
7. Select "Install Now".
8. Choose the Location and User information.
9. Wait.

### Debian
 
1. Select language.
2. Select layout.
3. Set hostname
4. Skip. (not need to make a domain name)
5. Set up root user.
6. Set up user information, with password.
7. Select Time Zone
8. Manual disc set up;  Reduce disk space; Create new partition, mark as root.

Laptops with very little amount of memory create also a swap partition.

If we are using UEFI:
Create a EFI partition
Create a main partition

If Bios:
Create a main partition with the boot flag. 

9. Continue without mirrors.
10. Opt out to provide statisics data for packages.
11. Chose a Window Manager
12. Install GRUB allongside Windows (if you want to). Choose the device where to install.
13. Reboot.

### Manjaro

1. Choose "Boot with open source drivers". If it does not work reboot and try with propietary.
2. Once you boot select the lenguage of the installation process, and then select "Probar Ubuntu".
3. Chose a network.
4. Start the installation.
5. Select layout/language. 
6. Manual install
7. Create partitions; mark ass root.

Laptops with very little amount of memory create also a swap partition.

If we are using UEFI:
Create a EFI partition
Create a main partition

If Bios:
Create a main partition with the boot flag. 

8. Set up user and superuser info.
9. Proceed with the install.


### Archlinux

1. Boot the iso.
2. Run: loadkeys es
3. If there is no network (ie wifi is needed) use iwctl. If it does not work 
use a USB with the eduroam script to configure it.
3. Run: fdisk # and create partitions
3. Run: archinstall
4. Select a language
5. Select a layout
6. Select a mirror region (a close country for better connectivity)
7. Selct a Drive(s)
8. Set the disk layout; use a free region to create the partitions

Laptops with very little amount of memory create also a swap partition.

If we are using UEFI:
Create a EFI partition
Create a main partition

If Bios:
Create a main partition with the boot flag. 

10. Grub install
11. Swap false (unless very low memory)
12. Set hostname
13. Set root password
14. Create user accounts and their passwords
15. Select a Profile, select desktop
16. Select the graphics driver
17. Audio: default
18. Aditional packages git and vim linux-firmware
19. Network configuration, use Network Manager.
20. Select timezone
21. Run the script

<!--fer una prova d'instal·lació abans-->

### Dualboot
### Boot al nou sistema
## Recomanacions
### Configuració Eduroam
### Àlies PRO1
### Programari recomanat
### On trobar informació
### Publicitar cursos futurs + enquesta
## Resolució de problemes
### GRUB Recovery
### Drivers Xarxa
### Drivers Gràfica
### Àudio
### Trackpad
